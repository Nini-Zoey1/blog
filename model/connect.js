const mongoose = require('mongoose');

const config = require('config');

mongoose.connect('mongodb://localhost/blog', { useUnifiedTopology: true, useNewUrlParser: true })
    .then(() => console.log('Connect DB Success!'))
    // .catch((e) => console.log(e))
    .catch(() => console.log('Connect DB Failed!'))