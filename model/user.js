const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
const Joi = require('joi');

mongoose.set('useCreateIndex', true);

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: 2,
        maxlength: 20
    },
    email: {
        type: String,
        unique: true,
    },
    password: {
        type: String,
        required: true
    },
    role: {
        type: String,
        required: true
    },
    state: {
        type: Number,
        default: 0
    }
});

const User = mongoose.model('User', userSchema);

async function createUser() {
    const salt = await bcrypt.genSalt(10);
    const pass = await bcrypt.hash('123456', salt);
    console.log(pass);

    const user = await User.create({
        username: 'test',
        email: 'test@gmail.com',
        password: pass,
        role: 'admin',
        state: 0
    }).then(() => {
        console.log('Create user successful!');
    }).catch(() => {
        console.log('Create user failed!');
    });
}

// createUser();

const validateUser = user => {
    const schema = {
        username: Joi.string().min(2).max(12).required(),
        email: Joi.string().email().required(),
        password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/).required(),
        role: Joi.string().valid('normal', 'admin').required(),
        state: Joi.number().valid(0, 1).required()
    };
    return Joi.validate(user, schema);
}

module.exports = {
    User,
    validateUser
}