const express = require('express');
const app = express();
const path = require('path');
const bodyParser = require('body-parser');
const session = require('express-session');
const template = require('art-template');
const dateFormat = require('dateformat');
const config = require('config');

const home = require('./route/home');
const admin = require('./route/admin');

require('./model/connect');
require('./model/user');

app.use(session({ secret: 'secret key' }))
app.use(bodyParser.urlencoded({ extended: false }))

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'art');
app.engine('art', require('express-art-template'));
template.defaults.imports.dateFormat = dateFormat;

app.use(express.static(path.join(__dirname, 'public')));

console.log(config.get('title'));


app.use('/admin', require('./middleware/loginGuard'));

app.use('/home', home);
app.use('/admin', admin);

app.use((err, req, res, next) => {
    console.log(err);

    const result = JSON.parse(err);
    // console.log(result);
    let params = [];
    for (let attr in result) {
        if (attr != 'path') {
            params.push(attr + '=' + result[attr]);
        }
    }
    return res.redirect(`${result.path}?${params.join('$')}`);
})

app.listen(80);
console.log('Server Start! Please click http://localhost');