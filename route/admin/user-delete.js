const { User } = require('../../model/user');

const userDelete = async(req, res) => {
    // console.log(req.query.id)
    await User.findOneAndDelete({ _id: String(req.query.id) });
    return res.redirect('/admin/user');
}

module.exports = userDelete;