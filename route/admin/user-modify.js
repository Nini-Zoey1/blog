const { User } = require('../../model/user');
const bcrypt = require('bcrypt');

const userModify = async(req, res, next) => {
    const { username, email, role, state, password } = req.body;

    const id = req.query.id;
    let user = await User.findOne({ _id: id });
    const isValid = await bcrypt.compare(password, user.password);
    if (isValid) {
        await User.updateOne({ _id: id }, {
            username: username,
            email: email,
            role: role,
            state: state
        })
        return res.redirect('/admin/user');
    } else {
        let obj = { path: '/admin/user-edit', message: 'Incorrect password!', id: id };
        next(JSON.stringify(obj));
    }
};

module.exports = userModify;