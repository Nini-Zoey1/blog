const bcrypt = require('bcrypt');

const { User } = require('../../model/user');

const login = async(req, res) => {
    const { email, password } = req.body;
    if (email.trim().length == 0 || password.trim().length == 0) {
        // return res.status(400).send('<h4>Incorrect email address or password!</h4>');
        return res.status(400).render('admin/error', { msg: 'Incorrect email address or password!' });
    }
    let user = await User.findOne({ email });
    if (user) {
        let isValid = await bcrypt.compare(password, user.password)
        if (isValid) {
            req.session.username = user.username;
            req.session.role = user.role;
            req.app.locals.userInfo = user;
            if (user.role == 'admin') {
                return res.redirect('/admin/user');
            } else {
                res.redirect('/home/');
            }

            // res.send('Login Successful!');
        } else {
            return res.status(400).render('admin/error', { msg: 'Incorrect email address or password!' });
        }
    } else {
        return res.status(400).render('admin/error', { msg: 'Incorrect email address or password!' });
    }

    // return res.render('admin/login');
};

module.exports = login;