const { Article } = require('../../model/article');
const pagination = require('mongoose-sex-page');
const article = async(req, res) => {
    const page = req.query.page;

    req.app.locals.currentLink = 'article';
    let articles = await pagination(Article).find().page(page).size(5).display(3).populate('author').exec();
    // res.send(articles);
    // let articles = await Article.find().populate('author');

    return res.render('admin/article.art', {
        articles: articles
    });
}

module.exports = article;