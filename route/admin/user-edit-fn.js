const { User, validateUser } = require('../../model/user');
const bcrypt = require('bcrypt');

const userEditFn = async(req, res, next) => {
    try {
        await validateUser(req.body)
    } catch (e) {
        let index = e.message.lastIndexOf("[");
        message = e.message.substring(index + 1, e.message.length - 1);
        return next(JSON.stringify({ path: '/admin/user-edit', message: message }));
        // return res.redirect(`/admin/user-edit?message=${message}`);
    }

    let user = await User.findOne({ email: req.body.email });

    if (user) {
        return next(JSON.stringify({ path: '/admin/user-edit', message: 'Email existed!' }))
            // return res.redirect(`/admin/user-edit?message=Email existed!`);
    }
    const salt = await bcrypt.genSalt(10);
    const password = await bcrypt.hash(req.body.password, salt);
    req.body.password = password;
    await User.create(req.body);
    return res.redirect('/admin/user');

    // res.end('ok');
}

module.exports = userEditFn;