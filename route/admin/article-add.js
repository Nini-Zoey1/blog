const formidable = require('formidable');
const path = require('path');
const { Article } = require('../../model/article');

// const reader = new FileReader();

const articleAdd = (req, res) => {
    const form = new formidable.IncomingForm();
    form.uploadDir = path.join(__dirname, '../', '../', 'public', 'uploads');
    form.keepExtensions = true;
    form.parse(req, async(err, fields, files) => {
        // 1. err存储错误信息
        // 2. fields对象类型，存储普通表单数据
        // 3. files对象类型，存储上传的二进制文件内容
        // res.send(files.cover.path.split('public')[1]);
        // res.send(fields);

        await Article.create({
            title: fields.title,
            author: fields.author,
            publishDate: fields.publishDate,
            cover: files.cover.path.split('public')[1],
            content: fields.content
        });
        return res.redirect('/admin/article');
    })

    // res.send('ok')
};

module.exports = articleAdd