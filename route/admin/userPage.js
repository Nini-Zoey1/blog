const { User } = require('../../model/user');

const userPage = async(req, res) => {

    req.app.locals.currentLink = 'user';

    let page = req.query.page || 1;
    let pagesize = 5;
    let count = await User.countDocuments({})
    let total = Math.ceil(count / pagesize);
    let start = (page - 1) * pagesize;
    let users = await User.find({}).limit(pagesize).skip(start);


    return res.render('admin/user', {
        users: users,
        page: page,
        total: total,
        count: count
    });
};

module.exports = userPage;