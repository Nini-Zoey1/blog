const { User } = require('../../model/user');

const userEdit = async(req, res) => {

    req.app.locals.currentLink = 'user';

    const { message, id } = req.query;
    if (id) {
        let user = await User.findOne({ _id: id });
        return res.render('admin/user-edit', {
            message: message,
            user: user,
            link: '/admin/user-modify?id=' + id,
            button: 'Edit'
        });
    } else {
        return res.render('admin/user-edit', {
            message: message,
            link: '/admin/user-edit',
            button: 'Submit'
        });
    }
}

module.exports = userEdit;