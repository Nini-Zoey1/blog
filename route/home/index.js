const { Article } = require('../../model/article');
const pagination = require('mongoose-sex-page');


const index = async(req, res) => {
    const page = req.query.page;
    const result = await pagination(Article).find().page(page).size(6).display(3).populate('author').exec();
    // return res.send(result);
    return res.render('home/default', {
        result: result
    });
    // res.end('Welcome to homepage!')
};
module.exports = index;