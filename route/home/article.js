const { Article } = require('../../model/article');
const { Comment } = require('../../model/comment');


const article = async(req, res) => {

    const id = req.query.id;
    let article = await Article.findOne({ _id: id }).populate('author');
    let comments = await Comment.find({ aid: id }).populate('uid');
    // return res.send(article);
    return res.render('home/article', {
        article: article,
        comments: comments
    });
    // res.end('Welcome to article!')
};
module.exports = article;