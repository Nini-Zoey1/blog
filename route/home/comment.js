const { Comment } = require('../../model/comment');

const comment = async(req, res) => {
    const { content, uid, aid } = req.body;

    await Comment.create({
        content: content,
        uid: uid,
        aid: aid,
        time: new Date()
    });

    return res.redirect('/home/article?id=' + aid);

};

module.exports = comment;